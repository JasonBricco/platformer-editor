﻿// Adventure

using UnityEngine;
using System.Text;
using System.IO;

public sealed class ErrorHandler : MonoBehaviour 
{
	private void Awake()
	{
		Application.logMessageReceived += HandleError;
	}

	private void HandleError(string logString, string stackTrace, LogType type)
	{
		if (type == LogType.Error || type == LogType.Exception || type == LogType.Assert)
		{
			Log(logString, stackTrace);
			Application.Quit();
		}
	}

	public static void Log(params string[] items)
	{
		StringBuilder text = new StringBuilder(System.DateTime.Now.ToString() + System.Environment.NewLine);

		for (int i = 0; i < items.Length; i++)
			text.AppendLine(items[i]);

		File.AppendAllText(Application.persistentDataPath + "/Log.txt", text.ToString() + System.Environment.NewLine);
	}
}
