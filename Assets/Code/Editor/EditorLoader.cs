﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.IO;

[InitializeOnLoadAttribute]
public static class EditorLoader
{
	static EditorLoader()
	{
		EditorApplication.playModeStateChanged += CheckSceneIntegrity;
	}

	private static void CheckSceneIntegrity(PlayModeStateChange state)
	{
		if (state == PlayModeStateChange.ExitingEditMode)
		{
			if (EditorSceneManager.GetActiveScene().buildIndex != 0)
				EditorSceneManager.OpenScene("Assets/Scenes/Loader.unity", OpenSceneMode.Single);
		}
	}
}
