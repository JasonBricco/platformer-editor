﻿// Adventure

using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using MoreMountains.Tools;

[RequireComponent(typeof(LevelLoader))]
public sealed class Map : MonoBehaviour
{
	public const int SizeInTiles = 512;
	public const int SizeInChunks = SizeInTiles / Chunk.Size;
	public const int TilesPerChunk = Chunk.Size * Chunk.Size;
	
	public const int LoadRange = Chunk.Size * 2;

	private Tile[] tiles = new Tile[SizeInTiles * SizeInTiles];
	private Chunk[] chunks = new Chunk[SizeInChunks * SizeInChunks];

	private List<Chunk> loadedChunks = new List<Chunk>(8);
	private List<Chunk> pendingUpdates = new List<Chunk>(8);

	[SerializeField] private ChunkRenderer rend;

	public static Map Instance { get; private set; }

	private void Awake()
	{
		Instance = this;
		TileManager.Init();
	}

	private void Start()
	{
		for (int y = 0; y < SizeInChunks; y++)
		{
			for (int x = 0; x < SizeInChunks; x++)
				chunks[y * SizeInChunks + x] = new Chunk(rend, x, y);
		}
	}

	public Vector2i ContainingChunk(Vector3 wPos)
	{
		return ContainingChunk(Mathf.RoundToInt(wPos.x), Mathf.RoundToInt(wPos.y));
	}

	public Vector2i ContainingChunk(int tileX, int tileY)
	{
		return new Vector2i(tileX >> Chunk.Shift, tileY >> Chunk.Shift) * Chunk.Size;
	}

	private Chunk GetChunk(Vector2i pos)
	{
		return GetChunk(pos.x, pos.y);
	}

	private Chunk GetChunk(int tileX, int tileY)
	{
		return GetChunkDirect(tileX >> Chunk.Shift, tileY >> Chunk.Shift);
	}

	private Chunk GetChunkDirect(int cX, int cY)
	{
		return chunks[cY * SizeInChunks + cX];
	}

	private void FlagChunkForUpdate(int x, int y)
	{
		Chunk chunk = GetChunk(x, y);

		if (!chunk.pendingUpdate)
		{
			pendingUpdates.Add(chunk);
			chunk.pendingUpdate = true;
		}
	}

	private void SetTileInternal(int x, int y, Tile tile)
	{
		if (tile.CanSet(x, y))
		{
			tiles[y * SizeInTiles + x] = tile;
			tile.OnSet(x, y);
			FlagChunkForUpdate(x, y);
		}
	}

	public void SetTile(int x, int y, Tile tile)
	{
		SetTileInternal(x, y, tile);
		UpdateChunks();
	}

	public void SetTileNoUpdate(int x, int y, Tile tile)
	{
		tiles[y * SizeInTiles + x] = tile;
		tile.OnSet(x, y);
	}

	public void SetTiles(List<Vector2i> positions, Tile tile)
	{
		for (int i = 0; i < positions.Count; i++)
		{
			Vector2i pos = positions[i];
			SetTileInternal(pos.x, pos.y, tile);
		}

		UpdateChunks();
	}

	public void SetTileInstances(List<TileInstance> instances)
	{
        for (int i = 0; i < instances.Count; i++)
        {
            TileInstance inst = instances[i];
            SetTileInternal(inst.pos.x, inst.pos.y, inst.tile);
        }

		UpdateChunks();
	}

	public void SetTileData(int x, int y, byte data)
	{
		tiles[y * SizeInTiles + x].data = data;
		FlagChunkForUpdate(x, y);
	}

	public void SetTileRotation(int x, int y, int rotation)
	{
		int index = y * SizeInTiles + x;
		tiles[index].rotation = (byte)rotation;
		tiles[index].OnRotate(x, y);
		FlagChunkForUpdate(x, y);
	}

	public Tile GetTile(Vector2i pos)
	{
		return GetTile(pos.x, pos.y);
	}

	public Tile GetTile(int x, int y)
	{
		return tiles[y * SizeInTiles + x];
	}

	public Tile GetTileDirect(int index)
	{
		return tiles[index];
	}

	public Tile GetTileSafe(int x, int y)
	{
		if (!InBounds(x, y)) 
			return TileType.Air;

		return GetTile(x, y);
	}

	public Tile GetTileSafe(Vector2i pos)
	{
		return GetTileSafe(pos.x, pos.y);
	}

	public Vector2i FindFirstSolidTile()
	{
		for (int y = 0; y < SizeInTiles; y++)
		{
			for (int x = 0; x < SizeInTiles; x++)
			{
				Tile tile = GetTile(x, y);

				if (tile.CanStandOn && GetTileSafe(x, y + 1).ID == 0 && GetTileSafe(x, y + 2).ID == 0)
					return new Vector2i(x, y);
			}
		}

		return new Vector2i(-1, -1);
	}

	public bool InBounds(int x, int y)
	{
		return x >= 0 && x < SizeInTiles && y >= 0 && y < SizeInTiles;
	}

	public void UpdateChunks()
	{
		for (int i = 0; i < pendingUpdates.Count; i++)
		{
            Chunk chunk = pendingUpdates[i];
            chunk.pendingUpdate = false;

			if (chunk.loaded)
            	rend.BuildMesh(chunk);
		}

		pendingUpdates.Clear();
	}

	private void Update()
	{
		Vector2i pos = ContainingChunk(Camera.main.transform.position);

		for (int i = loadedChunks.Count - 1; i >= 0; i--)
		{
			Chunk chunk = loadedChunks[i];
			Vector2i worldPos = chunk.worldPos;

			int xDiff = Mathf.Abs(worldPos.x - pos.x);
			int yDiff = Mathf.Abs(worldPos.y - pos.y);

			if (xDiff > LoadRange || yDiff > LoadRange)
			{
				chunk.Reset();
				loadedChunks.RemoveAt(i);
			}
		}

		for (int y = -LoadRange; y <= LoadRange; y += Chunk.Size)
		{
			for (int x = -LoadRange; x <= LoadRange; x += Chunk.Size)
			{
				Vector2i next = pos + new Vector2i(x, y);

				if (InBounds(next.x, next.y))
				{
					Chunk chunk = GetChunk(next);

					if (!chunk.loaded)
					{
						rend.BuildMesh(chunk);
						chunk.loaded = true;
						loadedChunks.Add(chunk);
					}
				}
			}
		}

		rend.Render(loadedChunks);
	}

	public byte[] Compress()
	{
		byte[] bytes = new byte[tiles.Length * 4];

		unchecked
		{
			int bi = 0;

			for (int i = 0; i < tiles.Length; i++)
			{
				Tile tile = tiles[i];
				bytes[bi] = (byte)(tile.ID >> 8);
				bytes[bi + 1] = (byte)(tile.ID);
				bytes[bi + 2] = tile.rotation;
				bytes[bi + 3] = tile.data;

				bi += 4;
			}
		}

		return CLZF2.Compress(bytes);
	}

	public void Load(string path)
	{
		if (File.Exists(path))
		{
			byte[] bytes = File.ReadAllBytes(path);
			byte[] data = CLZF2.Decompress(bytes);

			unchecked
			{
				int di = 0;

				for (int i = 0; i < tiles.Length; i++)
				{
					Tile tile = new Tile();
					tile.ID = (ushort)((data[di] << 8) | (data[di + 1]));
					tile.rotation = data[di + 2];
					tile.data = data[di + 3];
					tiles[i] = tile;
                    di += 4;
				}
			}
		}
	}

	public void Clear()
	{
		for (int i = 0; i < tiles.Length; i++)
			tiles[i] = TileType.Air;

		for (int i = 0; i < loadedChunks.Count; i++)
			loadedChunks[i].Reset();

		loadedChunks.Clear();
		pendingUpdates.Clear();
	}

	public Vector2i ClampToBounds(Vector2i value)
    {
        value.x = Mathf.Clamp(value.x, 0, SizeInTiles - 1);
        value.y = Mathf.Clamp(value.y, 0, SizeInTiles - 1);
        return value;
    }

	public Vector2i ScreenToTilePos(Vector3 screenPos)
	{
		return WorldToTilePos(Camera.main.ScreenToWorldPoint(screenPos));
	}

	public Vector2i WorldToTilePos(Vector3 worldPos)
	{
		return new Vector2i(Mathf.FloorToInt(worldPos.x), Mathf.FloorToInt(worldPos.y));
	}
}
