﻿// Adventure

using UnityEngine;
using System.Collections.Generic;
using Vectrosity;

public sealed class EditorCam : MonoBehaviour 
{
	[SerializeField] private float speed;
	[SerializeField] private float fastSpeed;
	[SerializeField] private float zoomSpeed;
	[SerializeField] private float smoothSpeed;
	[SerializeField] private float minOrtho;
	[SerializeField] private float maxOrtho;

	private Camera cam;

	private float targetOrtho;
	private float edgeOffsetX, edgeOffsetY;

	private VectorLine grid;

	private void Start()
	{
		cam = GetComponent<Camera>();
		targetOrtho = cam.orthographicSize;

		if (PlayerPrefs.HasKey("EditCamX"))
		{
			float x = PlayerPrefs.GetFloat("EditCamX");
			float y = PlayerPrefs.GetFloat("EditCamY");
			float z = PlayerPrefs.GetFloat("EditCamZ");
			transform.position = new Vector3(x, y, z);
		}

		List<Vector3> gridPoints = new List<Vector3>();

        for (int x = 0; x <= Map.SizeInTiles; x++)
        {
            gridPoints.Add(new Vector3(x, -0.0f, 0.0f));
            gridPoints.Add(new Vector3(x, Map.SizeInTiles, 0.0f));
        }

        for (int y = 0; y <= Map.SizeInTiles; y++)
        {
            gridPoints.Add(new Vector3(0.0f, y, 0.0f));
            gridPoints.Add(new Vector3(Map.SizeInTiles, y, 0.0f));
        }

        grid = new VectorLine("Grid", gridPoints, 1.0f);
        grid.color = Color.black;
        grid.Draw3DAuto();
	}

	private void LateUpdate()
	{
		Vector3 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
		float mul = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift) ? fastSpeed : speed;
		transform.Translate(input * mul * Time.deltaTime);

        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(pos.x, edgeOffsetX, Map.SizeInTiles - edgeOffsetX);
        pos.y = Mathf.Clamp(pos.y, edgeOffsetY, Map.SizeInTiles - edgeOffsetY);
        transform.position = pos;

		float scroll = Input.GetAxis("Zoom");

		if (!Mathf.Approximately(scroll, 0.0f))
		{
			targetOrtho -= scroll * zoomSpeed * Time.deltaTime;
			targetOrtho = Mathf.Clamp(targetOrtho, minOrtho, maxOrtho);
		}

		cam.orthographicSize = Mathf.MoveTowards(cam.orthographicSize, targetOrtho, smoothSpeed * Time.deltaTime);
		edgeOffsetY = cam.orthographicSize;
		edgeOffsetX = edgeOffsetY * Screen.width / Screen.height;
	}

	private void OnDestroy()
	{
		Vector3 pos = transform.position;
		PlayerPrefs.SetFloat("EditCamX", pos.x);
		PlayerPrefs.SetFloat("EditCamY", pos.y);
		PlayerPrefs.SetFloat("EditCamZ", pos.z);
	}
}
