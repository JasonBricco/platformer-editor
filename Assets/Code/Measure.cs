﻿// Adventure

using System.Diagnostics;
using Debug = UnityEngine.Debug;

public static class Measure
{
	private static Stopwatch watch = new Stopwatch();
	private static string sectionLabel;

	[Conditional("DEBUG")]
	public static void Start(string label)
	{
		sectionLabel = label;
		watch.Start();
	}

	[Conditional("DEBUG")]
	public static void Stop()
	{
		watch.Stop();
		Debug.Log(sectionLabel + "> MS: " + watch.Elapsed.TotalMilliseconds.ToString("F2"));
		watch.Reset();
	}

	[Conditional("DEBUG")]
	public static void Restart(string label)
	{
		Stop();
		Start(label);
	}
}
