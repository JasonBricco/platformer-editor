﻿// Jason Bricco 2017-2018

using UnityEngine;
using MoreMountains.CorgiEngine;

public sealed class GameCamera : CameraController
{
    protected override void Start() {}

    public void Init()
    {
		_camera = GetComponent<Camera>();
		FollowsPlayer = true;
		_currentZoom = MinimumZoom;
			
		if ((Level.Instance.Players == null) || (Level.Instance.Players.Count == 0))
		{
			Debug.LogWarning("No player found by the camera.");
			return;
		}

		_target = Level.Instance.Players[0].transform;

		if (_target.GetComponent<CorgiController>() == null)
		{
			Debug.LogWarning("Player has no Corgi Controller.");
			return;
		}

		_targetController = _target.GetComponent<CorgiController>();

		_levelBounds = Level.Instance.LevelBounds;

		if (Level.Instance.OneWayLevelMode != Level.OneWayLevelModes.None)
			_noGoingBackObject = Level.Instance.NoGoingBackObject;
		else _noGoingBackObject = null;
			
		_lastTargetPosition = _target.position;
		_offsetZ = (transform.position - _target.position).z;
		transform.parent = null;

		MakeCameraPixelPerfect();
		GetLevelBounds();	
    }

    protected override void FollowPlayer()
    {
        float xMoveDelta = (_target.position - _lastTargetPosition).x;
        bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > LookAheadTrigger;

        if (updateLookAheadTarget)
            _lookAheadPos = HorizontalLookDistance * Vector3.right * Mathf.Sign(xMoveDelta);
        else
            _lookAheadPos = Vector3.MoveTowards(_lookAheadPos, Vector3.zero, Time.deltaTime * ResetSpeed);

        Vector3 aheadTargetPos = _target.position + _lookAheadPos + Vector3.forward * _offsetZ + _lookDirectionModifier + CameraOffset;
        Vector3 newCameraPosition = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref _currentVelocity, CameraSpeed);
        Vector3 shakeFactorPosition = Vector3.zero;

        if (_shakeDuration > 0)
        {
            shakeFactorPosition = Random.insideUnitSphere * _shakeIntensity * _shakeDuration;
            _shakeDuration -= _shakeDecay * Time.deltaTime;
        }

        newCameraPosition = newCameraPosition + shakeFactorPosition;

        if (_camera.orthographic == true)
        {
            float posX, posY, posZ = 0f;

            if (_levelBounds.size != Vector3.zero)
            {
                posX = Mathf.Clamp(newCameraPosition.x, _xMin, _xMax);
                posY = Mathf.Max(newCameraPosition.y, _yMin);
            }
            else
            {
                posX = newCameraPosition.x;
                posY = newCameraPosition.y;
            }

            posZ = newCameraPosition.z;
            transform.position = new Vector3(posX, posY, posZ);
        }
        else transform.position = newCameraPosition;

        _lastTargetPosition = _target.position;
    }

    public override void OnMMEvent(CorgiEngineEvent e)
    {
        switch (e.EventType)
        {
            case CorgiEngineEventTypes.Respawn:
				_currentVelocity = Vector2.zero;
                TeleportCameraToTarget();
                FollowsPlayer = true;
                break;

            case CorgiEngineEventTypes.LevelStart:
                TeleportCameraToTarget();
                break;
        }
    }
}
