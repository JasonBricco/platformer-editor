﻿// Adventure

using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public sealed class LevelData
{
	public List<Vector2i> checkpoints;
	public string levelPath;
}
