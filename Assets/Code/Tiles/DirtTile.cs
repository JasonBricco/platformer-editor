﻿// Adventure

public sealed class DirtTile : NeighborTile 
{
	public override void UpdateAppearance(Tile tile, int x, int y)
	{
		bool airA, airB;
		GetAirNeighbors(tile, x, y, out airA, out airB);

        if (airA && airB) Map.Instance.SetTileData(x, y, 4);
		else if (airA) Map.Instance.SetTileData(x, y, 0);
		else if (airB) Map.Instance.SetTileData(x, y, 2);
        else Map.Instance.SetTileData(x, y, 1);
	}
}
