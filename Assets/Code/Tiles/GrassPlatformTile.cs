﻿// Adventure

public sealed class GrassPlatformTile : NeighborTile 
{
	public GrassPlatformTile()
	{
		Material = 1;
	}

    public override void UpdateAppearance(Tile tile, int x, int y)
    {
		bool airA, airB;
		GetAirNeighbors(tile, x, y, out airA, out airB);

		if (airA && airB) Map.Instance.SetTileData(x, y, 9);
		else if (airA) Map.Instance.SetTileData(x, y, 6);
		else if (airB) Map.Instance.SetTileData(x, y, 8);
		else Map.Instance.SetTileData(x, y, 7);
	}
}
