﻿// Adventure

using System.Runtime.CompilerServices;

public static class TileType
{
    public static Tile Air { get { return new Tile(0); } }
    public static Tile Grass { get { return new Tile(1); } }
    public static Tile Dirt { get { return new Tile(2); } }
    public static Tile GrassCorner { get { return new Tile(3); } }
    public static Tile GrassPlatform { get { return new Tile(4); } }
    public static Tile GrassSlope { get { return new Tile(5); } }
    public static Tile GrassOneWay { get { return new Tile(6); } } 
    public static Tile Kill { get { return new Tile(7); } }
}

public static class TileManager
{
	private static TileBase[] info = new TileBase[8];

    public static void Init()
    {
        info[0] = new AirTile();
        info[1] = new GrassTile();
        info[2] = new DirtTile();
        info[3] = new GrassCornerTile();
        info[4] = new GrassPlatformTile();
        info[5] = new GrassSlopeTile();
        info[6] = new GrassOneWayTile();
        info[7] = new KillTile();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static TileBase GetInfo(ushort ID)
    {
        return info[ID];
    }
}
