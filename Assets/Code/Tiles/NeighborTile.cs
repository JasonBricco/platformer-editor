﻿// Adventure

public class NeighborTile : TileBase 
{
	public override float GetTexture(ushort data)
	{
		return (float)data;
	}

	public override void OnSet(int x, int y, Tile tile)
	{
		Update(tile, x, y);
	}

    public override void OnRotate(int x, int y, Tile tile)
    {
        Update(tile, x, y);
    }

    private void Update(Tile tile, int x, int y)
	{
		UpdateNeighborAppearances(x, y);
		UpdateAppearance(tile, x, y);
	}

	protected void GetAirNeighbors(Tile tile, int x, int y, out bool a, out bool b)
	{
		switch (tile.rotation)
		{
			case 0:
				a = Map.Instance.GetTileSafe(x - 1, y).ID == 0;
				b = Map.Instance.GetTileSafe(x + 1, y).ID == 0;
				break;

			case 1:
				a = Map.Instance.GetTileSafe(x, y - 1).ID == 0;
				b = Map.Instance.GetTileSafe(x, y + 1).ID == 0;
				break;

			case 2:
				b = Map.Instance.GetTileSafe(x - 1, y).ID == 0;
				a = Map.Instance.GetTileSafe(x + 1, y).ID == 0;
				break;

			case 3:
				b = Map.Instance.GetTileSafe(x, y - 1).ID == 0;
				a = Map.Instance.GetTileSafe(x, y + 1).ID == 0;
				break;

			default:
				a = false;
				b = false;
				break;
		}
	}
}
