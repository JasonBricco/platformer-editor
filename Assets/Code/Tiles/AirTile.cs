﻿// Adventure

public sealed class AirTile : TileBase
{
    public AirTile()
    {
        Collider = ColliderType.None;
        CanStandOn = false;
        Visible = false;
    }
}
