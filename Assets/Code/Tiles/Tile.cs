﻿// Adventure

using System.Runtime.CompilerServices;

public struct Tile 
{
    public ushort ID;
    public byte data;
    public byte rotation;

    public Tile(ushort ID, byte data = 0, byte rotation = 0)
    {
        this.ID = ID;
        this.data = data;
        this.rotation = rotation;
    }

    public bool Visible
    {
        get { return TileManager.GetInfo(ID).Visible; }
    }

    public int Material
    {
        get { return TileManager.GetInfo(ID).Material; } 
    }

    public bool AllowRotation
    {
        get { return TileManager.GetInfo(ID).AllowRotation; }
    }

    public bool CanStandOn
    {
        get { return TileManager.GetInfo(ID).CanStandOn; }
    }

    public ColliderType Collider
    {
        get { return TileManager.GetInfo(ID).Collider; }
    }

    public bool HasCollider
    {
        get { return Collider != ColliderType.None; }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public float GetTexture()
    {
        return TileManager.GetInfo(ID).GetTexture(data);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void OnSet(int x, int y)
    {
        TileManager.GetInfo(ID).OnSet(x, y, this);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool CanSet(int x, int y)
	{
		return TileManager.GetInfo(ID).CanSet(x, y, this);
	}

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void OnRotate(int x, int y) 
    {
        TileManager.GetInfo(ID).OnRotate(x, y, this);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void UpdateAppearance(int x, int y) 
    {
        TileManager.GetInfo(ID).UpdateAppearance(this, x, y);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void OnTrigger()
    {
        TileManager.GetInfo(ID).OnTrigger();
    }
}
