﻿// Adventure

public sealed class GrassSlopeTile : TileBase 
{
	public GrassSlopeTile()
	{
		Material = 1;
		Collider = ColliderType.Slope;
	}

    public override float GetTexture(ushort data)
	{
		return 10.0f;
	}
}
