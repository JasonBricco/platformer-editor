﻿// Adventure

public sealed class KillTile : TileBase 
{
	public KillTile()
	{
		Material = 1;
        Collider = ColliderType.Trigger;
		Visible = !GameController.PlayMode;
		CanStandOn = false;
        AllowRotation = false;
	}

	public override float GetTexture(ushort data)
	{
		return 13.0f;
	}

	public override void OnTrigger()
	{
		Level.Instance.KillPlayer();
	}
}
