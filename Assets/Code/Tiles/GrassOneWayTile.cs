﻿// Adventure

public sealed class GrassOneWayTile : NeighborTile 
{
	public GrassOneWayTile()
	{
		Material = 1;
		AllowRotation = false;
        Collider = ColliderType.Edge;
	}

	public override void UpdateAppearance(Tile tile, int x, int y)
	{
		bool airA, airB;
		GetAirNeighbors(tile, x, y, out airA, out airB);

        if (airA && airB) Map.Instance.SetTileData(x, y, 12);
		else if (airA) Map.Instance.SetTileData(x, y, 3);
		else if (airB) Map.Instance.SetTileData(x, y, 5);
		else Map.Instance.SetTileData(x, y, 4);
	}
}
