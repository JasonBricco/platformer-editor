﻿// Adventure

public class TileBase
{
	public int Material { get; protected set; } = 0;
	public bool AllowRotation { get; protected set; } = true;
	public ColliderType Collider { get; protected set; } = ColliderType.Box;
	public bool Visible { get; protected set; } = true;
	public bool CanStandOn { get; protected set; } = true;

	public virtual float GetTexture(ushort data)
	{
		return 0.0f;
	}

	public virtual void OnSet(int x, int y, Tile tile) 
	{
		UpdateNeighborAppearances(x, y);
	}

	public virtual void OnRotate(int x, int y, Tile tile) {}
	
	public virtual void UpdateAppearance(Tile tile, int x, int y) {}

	public virtual bool CanSet(int x, int y, Tile tile)
	{
		return true;
	}

	protected void UpdateNeighborAppearances(int x, int y)
	{
		Map.Instance.GetTileSafe(x - 1, y).UpdateAppearance(x - 1, y);
        Map.Instance.GetTileSafe(x + 1, y).UpdateAppearance(x + 1, y);
        Map.Instance.GetTileSafe(x, y - 1).UpdateAppearance(x, y - 1);
        Map.Instance.GetTileSafe(x, y + 1).UpdateAppearance(x, y + 1);
	}

	public virtual void OnTrigger() {}
}
