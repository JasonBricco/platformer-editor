﻿// Adventure

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using System.Collections;

public class MainMenu : MonoBehaviour 
{
	[SerializeField] private GameObject mainButtons;
	[SerializeField] private GameObject message;

	public void OnClickPlay()
	{
		SceneManager.LoadScene(2);
	}

    public void OnClickDeleteData()
    {
        Directory.Delete(Application.persistentDataPath, true);
        PlayerPrefs.DeleteAll();
		ShowMessage("Data deleted successfully.", 2.0f);
    }

	public void OnClickQuit()
	{
		Application.Quit();
	}

	private void ShowMessage(string text, float time = 2.0f)
    {
        StopAllCoroutines();
        StartCoroutine(ShowMessageRoutine(text, time));
    }

    private IEnumerator ShowMessageRoutine(string text, float time)
    {
        message.GetComponent<Text>().text = text;
        message.SetActive(true);
        yield return new WaitForSeconds(time);
        message.SetActive(false);
    }
}
