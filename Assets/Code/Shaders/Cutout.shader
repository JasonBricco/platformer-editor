﻿// (c) Jason Bricco 2017-2018

Shader "Custom/Cutout" 
{
	Properties 
	{
		_TexArray ("Array", 2DArray) = "" {}
	}
	
	SubShader 
	{
		Pass
		{
			Tags { "Queue" = "Transparent" "RenderType" = "TransparentCutout" }
			Blend SrcAlpha OneMinusSrcAlpha
			LOD 200
			Lighting Off
			
			CGPROGRAM
			#pragma vertex vert
		    #pragma fragment frag
		    #pragma target 4.0

		    #include "UnityCG.cginc"

			struct VertexIn
			{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
				float4 col : COLOR;
			};

			struct VertexOut
			{
				float4 pos : SV_POSITION;
				float4 uv : TEXCOORD0;
				float4 col : COLOR;
			};

			UNITY_DECLARE_TEX2DARRAY(_TexArray);

			VertexOut vert(VertexIn v)
			{
				VertexOut o;

				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord; 
				o.col = v.col;

				return o;
			}

			float4 frag(VertexOut i) : COLOR
			{
				fixed4 col = UNITY_SAMPLE_TEX2DARRAY(_TexArray, i.uv.xyz) * i.col;
				col.rgb *= col.a;
				clip(col.a - 0.01f);
				return col;
			}
			
			ENDCG
		}
	}
}
