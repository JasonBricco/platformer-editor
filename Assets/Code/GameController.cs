﻿// Adventure

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using System.Collections;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public static class Scenes 
{ 
	public const int Loader = 0, Menu = 1, Editor = 2, Game = 3;
}

public struct SceneChangeEvent
{
	public int prev, next;

	public SceneChangeEvent(int prev, int next)
	{
		this.prev = prev;
		this.next = next;
	}
}

public sealed class GameController : GameManager 
{
    public static new GameController Instance { get; private set; }

	[SerializeField] private Material fadeMaterial;
	private Color fadeColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);

	public static bool PlayMode
	{
		get { return SceneManager.GetActiveScene().buildIndex == Scenes.Game; }
	}
	
	protected override void Awake()
	{
        Instance = this;
        DontDestroyOnLoad(gameObject);
		Screen.SetResolution(960, 540, false);
		SceneManager.LoadScene(Scenes.Menu);
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			switch (SceneManager.GetActiveScene().buildIndex)
			{
				case Scenes.Editor:
					ChangeScene(Scenes.Menu);
					break;

				case Scenes.Game:
					ChangeScene(Scenes.Editor);
					break;
			}
		}
	}

	public void ChangeScene(int next)
	{
		SceneChangeEvent e = new SceneChangeEvent(SceneManager.GetActiveScene().buildIndex, next);
		MMEventManager.TriggerEvent(e);
		SceneManager.LoadSceneAsync(next, LoadSceneMode.Single);
	}

	public void SetFade(float r, float g, float b, float a)
	{
		fadeColor = new Color(r, g, b, a);
	}

	public void GradualFade(float time, Color target, UnityAction callback)
	{
		if (fadeColor != target)
			StartCoroutine(DoGradualFade(time, target, callback));
	}

	private IEnumerator DoGradualFade(float time, Color target, UnityAction callback)
	{
		float mul = 1.0f / time;
		float t = 0.0f; 

		while (t < 1.0f)
		{
			fadeColor = Color.Lerp(fadeColor, target, t);
			t += (Time.unscaledDeltaTime * mul);
			yield return null;
		}

		fadeColor = target;

		if (callback != null)
			callback.Invoke();
	}

	private void OnPostRender()
	{
		if (fadeColor.a == 0.0f) return;

		fadeMaterial.SetColor("_Color", fadeColor);

		GL.PushMatrix();
		GL.LoadOrtho();

		fadeMaterial.SetPass(0);
		GL.Begin(GL.QUADS);

		GL.Vertex3(0.0f, 0.0f, 0.1f);
		GL.Vertex3(1.0f, 0.0f, 0.1f);
		GL.Vertex3(1.0f, 1.0f, 0.1f);
		GL.Vertex3(0.0f, 1.0f, 0.1f);

		GL.End();
		GL.PopMatrix(); 
	}
}
