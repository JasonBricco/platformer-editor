﻿// Adventure

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Vectrosity;
using MoreMountains.Tools;
using static Utils;

public enum EditMode
{
    Normal,
    Selection,
    Checkpoints,
    FloodFill,
    RectFill
}

public sealed class EditSelection
{
    public VectorLine line;
    public Vector2i startTile, endTile;
    public float left, right, up, down;
    public bool shiftHeld;

    public EditSelection(Vector2i startTile)
    {
        line = new VectorLine("Selection", new List<Vector3>(5), 2.0f, LineType.Continuous);

        this.startTile = startTile;
        left = startTile.x;
        right = left + 1.0f;
        down = startTile.y;
        up = down + 1.0f;
    }
}

public sealed class Modification
{
    public List<TileInstance> prevBlocks = new List<TileInstance>();
    public List<TileInstance> newBlocks = new List<TileInstance>();

    public Modification() {}

    public Modification(List<TileInstance> prevBlocks, List<TileInstance> newBlocks)
    {
        this.prevBlocks = prevBlocks;
        this.newBlocks = newBlocks;
    }

    public void Add(TileInstance toAdd, TileInstance prev)
    {
        prevBlocks.Add(prev);
        newBlocks.Add(toAdd);
    }
}

public sealed class MapBuilder : MonoBehaviour, MMEventListener<GameLoadedEvent>, MMEventListener<SceneChangeEvent>
{
    public GraphicRaycaster raycaster;

    [SerializeField] private Transform message;
    [SerializeField] private GameObject mainButtons;
    [SerializeField] private GameObject optionsPanel;
    [SerializeField] private GameObject levelButtonPrefab;
    [SerializeField] private GameObject levelsPanel;
    [SerializeField] private Button[] icons;
    [SerializeField] private Button activeTileButton;
    [SerializeField] private Button levelsPageUp;
    [SerializeField] private Button levelsPageDown;
    [SerializeField] private Text levelName;
    [SerializeField] private GameObject levelPropsPanel;
    [SerializeField] private InputField levelNameInput;

    private Map map;

    private ushort activeTile;

    private EditMode mode = EditMode.Normal;

    private bool validSelection;
    private int selectionID;
    private bool waitForCtrl, selectFinished;
    private List<EditSelection> selections = new List<EditSelection>();

    private Rect screenRect = new Rect(0.0f, 0.0f, Screen.width, Screen.height);

    private List<Vector2i> checkpoints = new List<Vector2i>();
    private List<VectorLine> checkpointMarks = new List<VectorLine>();

    private bool cursorInvalid;

    private List<TileInstance> copyList = new List<TileInstance>();

    private bool wasFocused = true;

    private UndoStack<Modification> undoStack = new UndoStack<Modification>(5);
    private UndoStack<Modification> redoStack = new UndoStack<Modification>(5);

    private const int LevelsPerPage = 13;
    private int currentPage = 0;
    private string currentLevel = null;
    private string[] levelsInWorld;

    private Modification normalEditTileModifications;

    private string CurrentLevel
    {
        get { return currentLevel; }
        set 
        {
            currentLevel = value;

            if (currentLevel != null)
                levelName.text = new DirectoryInfo(CurrentLevel).Name;
        }
    }

    LevelData levelData = new LevelData();

    private void Awake()
    {
        map = Map.Instance;
        this.MMEventStartListening<GameLoadedEvent>();
        this.MMEventStartListening<SceneChangeEvent>();
    }

    private void Start()
    {     
        activeTile = ushort.Parse(activeTileButton.name);
        activeTileButton.interactable = false;
    }

    public void SetActiveTile(Button button)
    {
        if (activeTileButton != null)
            activeTileButton.interactable = true;

        activeTile = ushort.Parse(button.name);
        button.interactable = false;
        activeTileButton = button;

        if (mode == EditMode.Checkpoints)
            SetNormalMode();
    }

    private void RegisterUndo(Modification modification)
    {
        undoStack.Push(modification);
        redoStack.Clear();
    }

    private void RegisterUndo(TileInstance prevBlock, TileInstance newBlock)
    {
        List<TileInstance> prevBlocks = new List<TileInstance>();
        List<TileInstance> newBlocks = new List<TileInstance>();

        prevBlocks.Add(prevBlock);
        newBlocks.Add(newBlock);

        RegisterUndo(new Modification(prevBlocks, newBlocks));
    }

    private void RegisterUndo(List<TileInstance> prevBlocks, List<TileInstance> newBlocks)
    {
        RegisterUndo(new Modification(prevBlocks, newBlocks));
    }

    private void Undo()
    {
        if (undoStack.Count > 0)
        {
            Modification mod = undoStack.Pop();
            map.SetTileInstances(mod.prevBlocks);
            redoStack.Push(mod);
        }
    }

    private void Redo()
    {
        if (redoStack.Count > 0)
        {
            Modification mod = redoStack.Pop();
            map.SetTileInstances(mod.newBlocks);
            undoStack.Push(mod);
        }
    }

    private void NormalEdit(Vector2i mouseTile)
    {
        if (cursorInvalid) return;

        if (Input.GetMouseButton(0))
        {
            Tile current = map.GetTile(mouseTile);
            Tile toAdd = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift) ? TileType.Air : new Tile(activeTile);

            if (current.ID != toAdd.ID)
            {
                map.SetTile(mouseTile.x, mouseTile.y, toAdd);

                if (normalEditTileModifications == null)
                    normalEditTileModifications = new Modification();

                normalEditTileModifications.Add(new TileInstance(toAdd, mouseTile), new TileInstance(current, mouseTile));
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (normalEditTileModifications != null)
            {
                RegisterUndo(normalEditTileModifications);
                normalEditTileModifications = null;
            }
        }
    }

    private void CheckpointEdit(Vector2i pos)
    {
        if (cursorInvalid) return;

        if (Input.GetMouseButtonDown(0))
        {
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                int index = checkpoints.IndexOf(pos);

                if (index != -1)
                {
                    checkpoints.RemoveAt(index);

                    VectorLine cp = checkpointMarks[index];
                    VectorLine.Destroy(ref cp);

                    checkpointMarks.RemoveAt(index);
                }
            }
            else AddCheckpoint(pos);
        }
    }

    private void FloodFillEdit(Vector2i mouseTile)
    {
        if (!cursorInvalid && Input.GetMouseButtonDown(0))
        {
            Tile tileToAdd;

            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                tileToAdd = TileType.Air;
            else tileToAdd = new Tile(activeTile);

            Tile tileToReplace = map.GetTile(mouseTile.x, mouseTile.y);

            List<Vector2i> tiles = new List<Vector2i>();
            HashSet<Vector2i> checkSet = new HashSet<Vector2i>();

            tiles.Add(mouseTile);
            checkSet.Add(mouseTile);

            int count = 0;

            for (int i = 0; i < tiles.Count; i++)
            {
                Vector2i current = tiles[i];

                for (int j = 0; j < 4; j++)
                {
                    Vector2i next = current + Vector2i.Directions[j];

                    if (!map.InBounds(next.x, next.y) || checkSet.Contains(next))
                        continue;

                    Tile nextTile = map.GetTile(next);

                    if (tileToReplace.ID == nextTile.ID)
                    {
                        checkSet.Add(next);
                        tiles.Add(next);
                        count++;
                    }
                }
            }

            map.SetTiles(tiles, tileToAdd);

            List<TileInstance> prevList = new List<TileInstance>(tiles.Count);
            List<TileInstance> newList = new List<TileInstance>(tiles.Count);

            for (int i = 0; i < tiles.Count; i++)
            {
                prevList.Add(new TileInstance(tileToReplace, tiles[i]));
                newList.Add(new TileInstance(tileToAdd, tiles[i]));
            }

            RegisterUndo(prevList, newList);
        }
    }

    private void MakeSelection(bool clearOld = false)
    {
        if (!cursorInvalid && Input.GetMouseButtonDown(0))
        {
            if (clearOld && selectFinished) ClearSelections();
            selections.Add(new EditSelection(map.ScreenToTilePos(Input.mousePosition)));
            validSelection = true;
            selectionID = selections.Count - 1;
            raycaster.enabled = false;
        }

        if (Input.GetMouseButton(0))
        {
            if (validSelection)
            {
                EditSelection selection = selections[selectionID];

                selection.endTile = map.ScreenToTilePos(Input.mousePosition);
                selection.endTile = map.ClampToBounds(selection.endTile);

                Vector3 selectStart = new Vector3(selection.left, selection.down, -1.0f);
                Vector3 selectEnd = new Vector3(selectStart.x + 1.0f, selectStart.y + 1.0f, -1.0f);

                if (selection.endTile != selection.startTile)
                {
                    float endLeft = selection.endTile.x;
                    float endRight = endLeft + 1.0f;
                    float endDown = selection.endTile.y;
                    float endUp = endDown + 1.0f;

                    int xDiff = selection.endTile.x - selection.startTile.x;
                    int yDiff = selection.endTile.y - selection.startTile.y;

                    if (xDiff < 0)
                    {
                        selectStart.x = selection.right;
                        selectEnd.x = endLeft;
                    }
                    else
                    {
                        selectStart.x = selection.left;
                        selectEnd.x = endRight;
                    }

                    if (yDiff < 0)
                    {
                        selectStart.y = selection.up;
                        selectEnd.y = endDown;
                    }
                    else
                    {
                        selectStart.y = selection.down;
                        selectEnd.y = endUp;
                    }
                }

                selection.line.MakeRect(selectStart, selectEnd);
                selection.line.Draw3D();
            }
        }

        if (validSelection && Input.GetMouseButtonUp(0))
        {
            selections[selectionID].shiftHeld = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);

            if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
                waitForCtrl = true;
            else
            {
                selectFinished = true;
                raycaster.enabled = true;
            }
        }

        if (waitForCtrl && (Input.GetKeyUp(KeyCode.LeftControl) || Input.GetKeyUp(KeyCode.RightControl)))
        {
            waitForCtrl = false;
            selectFinished = true;
            raycaster.enabled = true;
        }
    }

    private void ClearSelections()
    {
        for (int i = 0; i < selections.Count; i++)
            VectorLine.Destroy(ref selections[i].line);

        validSelection = false;
        selectFinished = false;
        selections.Clear();
        selectionID = 0;
    }

    private RectInt GetFillRect(EditSelection selection)
    {
        RectInt rect = new RectInt();
        rect.xMin = Mathf.Min(selection.startTile.x, selection.endTile.x);
        rect.xMax = Mathf.Max(selection.startTile.x, selection.endTile.x);
        rect.yMin = Mathf.Min(selection.startTile.y, selection.endTile.y);
        rect.yMax = Mathf.Max(selection.startTile.y, selection.endTile.y);
        return rect;
    }

    private void RectFillEdit()
    {
        MakeSelection();

        if (selectFinished)
        {
            List<TileInstance> prevList = new List<TileInstance>(256);
            List<TileInstance> newList = new List<TileInstance>(256);

            for (int i = 0; i < selections.Count; i++)
            {
                Tile tile = selections[i].shiftHeld ? TileType.Air : new Tile(activeTile);
                RectInt fillRect = GetFillRect(selections[i]);

                for (int y = fillRect.yMin; y <= fillRect.yMax; y++)
                {
                    for (int x = fillRect.xMin; x <= fillRect.xMax; x++)
                    {
                        Vector2i pos = new Vector2i(x, y);
                        prevList.Add(new TileInstance(map.GetTile(pos), pos));
                        newList.Add(new TileInstance(tile, pos));
                    }
                }
            }

            map.SetTileInstances(newList);
            RegisterUndo(prevList, newList);
            ClearSelections();
        }
    }

    private void SelectionEdit(Vector2i pos)
    {
        if (copyList.Count == 0)
        {
            MakeSelection(true);

            if (selectFinished)
            {
                raycaster.enabled = false;

                if (Input.GetKeyDown(KeyCode.C))
                {
                    List<RectInt> fillRects = new List<RectInt>(selections.Count);
                    Vector2i min = new Vector2i(int.MaxValue, int.MaxValue);

                    for (int i = 0; i < selections.Count; i++)
                    {
                        RectInt fillRect = GetFillRect(selections[i]);
                        fillRects.Add(fillRect);

                        if (fillRect.min.x < min.x) min.x = fillRect.min.x;
                        if (fillRect.min.y < min.y) min.y = fillRect.min.y;
                    }

                    for (int i = 0; i < fillRects.Count; i++)
                    {
                        RectInt fillRect = fillRects[i];

                        for (int y = fillRect.yMin; y <= fillRect.yMax; y++)
                        {
                            for (int x = fillRect.xMin; x <= fillRect.xMax; x++)
                            {
                                Vector2i offset = new Vector2i(x - min.x, y - min.y);
                                TileInstance inst = new TileInstance(map.GetTile(new Vector2i(x, y)), offset);
                                copyList.Add(inst);
                            }
                        }

                        if (selections[i].shiftHeld)
                        {
                            for (int y = fillRect.yMin; y <= fillRect.yMax; y++)
                            {
                                for (int x = fillRect.xMin; x <= fillRect.xMax; x++)
                                    map.SetTile(x, y, TileType.Air);
                            }
                        }
                    }

                    ShowMessage("Tiles copied. Click to paste, or right click to clear the clipboard.", 3.5f);
                    ClearSelections();
                }
            }
        }
        else
        {
            if (!cursorInvalid && Input.GetMouseButtonDown(0))
            {
                List<TileInstance> prevList = new List<TileInstance>(copyList.Count);
                List<TileInstance> newList = new List<TileInstance>(copyList.Count);

                for (int i = 0; i < copyList.Count; i++)
                {
                    TileInstance inst = copyList[i];
                    Vector2i p = pos + inst.pos;

                    if (map.InBounds(p.x, p.y))
                    {
                        prevList.Add(new TileInstance(map.GetTile(p), p));
                        newList.Add(new TileInstance(inst.tile, p));
                    }
                }

                map.SetTileInstances(newList);
                RegisterUndo(prevList, newList);
            }

            if (Input.GetMouseButtonDown(1))
            {
                ShowMessage("Copying canceled.");
                ClearSelections();
                copyList.Clear();
                raycaster.enabled = true;
            }
        }
    }

    private void HandleRotation(Vector2i tileMousePos)
    {
        if (!cursorInvalid && Input.GetMouseButtonDown(1))
        {
            bool rotatedSelection = false;

            if (selectFinished)
            {
                List<RectInt> fillRects = new List<RectInt>();
                bool allowRotate = false;

                for (int i = 0; i < selections.Count; i++)
                {
                    RectInt fillRect = GetFillRect(selections[i]);
                    fillRects.Add(fillRect);

                    RectInt checkRect = new RectInt();
                    checkRect.xMin = fillRect.xMin;
                    checkRect.xMax = fillRect.xMax + 1;
                    checkRect.yMin = fillRect.yMin;
                    checkRect.yMax = fillRect.yMax + 1;

                    if (checkRect.Contains(new Vector2Int(tileMousePos.x, tileMousePos.y)))
                        allowRotate = true;
                }

                if (allowRotate)
                {
                    for (int i = 0; i < fillRects.Count; i++)
                    {
                        RectInt fillRect = fillRects[i];

                        for (int y = fillRect.yMin; y <= fillRect.yMax; y++)
                        {
                            for (int x = fillRect.xMin; x <= fillRect.xMax; x++)
                            {
                                Tile tile = map.GetTile(x, y);

                                if (tile.AllowRotation)
                                    map.SetTileRotation(x, y, (tile.rotation + 1) % 4);
                            }
                        }

                        rotatedSelection = true;
                    }
                }
                else 
                {
                    raycaster.enabled = true;
                    ClearSelections();
                    SetNormalMode();
                }
            }

            if (!rotatedSelection)
            {
                Tile tile = map.GetTile(tileMousePos);

                if (tile.AllowRotation)
                    map.SetTileRotation(tileMousePos.x, tileMousePos.y, (tile.rotation + 1) % 4);
            }

            map.UpdateChunks();
        }
    }

    private void Update()
    {
        if (levelPropsPanel.activeSelf == true)
        {
            if (Input.GetKeyDown(KeyCode.Return))
                OnLevelNameApplied();
        }

        if (GameController.Instance.Paused) return;

        Vector3 mousePos = Input.mousePosition;

        cursorInvalid = EventSystem.current.IsPointerOverGameObject() || !screenRect.Contains(mousePos);

        if (Application.isFocused && !wasFocused)
            cursorInvalid = true;

        wasFocused = Application.isFocused;

        Vector2i tileMousePos = map.ScreenToTilePos(mousePos);

        if (!map.InBounds(tileMousePos.x, tileMousePos.y))
            cursorInvalid = true;

        HandleRotation(tileMousePos);

        switch (mode)
        {
            case EditMode.Normal:
                NormalEdit(tileMousePos);
                break;

            case EditMode.Selection:
                SelectionEdit(tileMousePos);
                break;

            case EditMode.Checkpoints:
                CheckpointEdit(tileMousePos);
                break;

            case EditMode.FloodFill:
                FloodFillEdit(tileMousePos);
                break;

            case EditMode.RectFill:
                RectFillEdit();
                break;
        }

        if (Input.GetKeyDown(KeyCode.Z)) Undo();
        if (Input.GetKeyDown(KeyCode.X)) Redo();

        if (CtrlPressed() && Input.GetKeyDown(KeyCode.S))
            Save(true);
    }

    private void AddCheckpoint(Vector2i tilePos)
    {
        VectorLine line = new VectorLine("Checkpoint", new List<Vector3>(), 1.0f);
        line.MakeText("S", new Vector3(tilePos.x + 0.25f, tilePos.y + 1.0f, -1.0f), 1.0f);
        line.Draw3D();

        checkpointMarks.Add(line);
        checkpoints.Add(tilePos);
    }

    public void OnMMEvent(GameLoadedEvent e)
    {
        LevelData data = e.data;

        if (data != null)
        {
            map.Load(e.data.levelPath);

            for (int i = 0; i < data.checkpoints.Count; i++)
                AddCheckpoint(data.checkpoints[i]);

            if (data.levelPath != null)
                CurrentLevel = Path.GetDirectoryName(data.levelPath);
        }
        else 
        {
            GameController.Instance.Pause();
            levelPropsPanel.SetActive(true);
            levelNameInput.ActivateInputField();
        }
    }

    public void OnLevelNameApplied()
    {
        if (levelNameInput.text.Length > 0)
        {
            CurrentLevel = Application.persistentDataPath + "/World/" + levelNameInput.text;
            GameController.Instance.UnPause();
            levelPropsPanel.SetActive(false);
        }
    }

    public void OnMMEvent(SceneChangeEvent e)
    {
        PlayerPrefs.SetString("LastLevel", currentLevel);
        Save(false);
    }

    public void OnClickOptions(GameObject options)
    {
        this.optionsPanel = options;
        mainButtons.SetActive(false);
        optionsPanel.SetActive(true);
    }

    public void OnClickSave()
    {
        Save(true);
        optionsPanel.SetActive(false);
        mainButtons.SetActive(true);
    }

    public void OnClickPlay()
    {
        if (checkpoints.Count == 0)
        {
            Vector2i solid = map.FindFirstSolidTile();

            if (solid.x != -1) 
                AddCheckpoint(solid + Vector2i.Up);
        }

        GameController.Instance.ChangeScene(Scenes.Game);
    }

    public void OnClickBackup()
    {
        Save(false);

        string targetPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/GameBackup/";

        if (Directory.Exists(targetPath))
            Directory.Delete(targetPath, true);

        DirectoryInfo source = new DirectoryInfo(Application.persistentDataPath + "/World/");
        DirectoryInfo dest = new DirectoryInfo(targetPath);

        CopyDir(source, dest);

        optionsPanel.SetActive(false);
        mainButtons.SetActive(true);

        ShowMessage("Backup successful.");
    }

    private void CopyDir(DirectoryInfo source, DirectoryInfo target)
    {
        Directory.CreateDirectory(target.FullName);

        foreach (FileInfo fi in source.GetFiles())
            fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);

        foreach (DirectoryInfo subDir in source.GetDirectories())
        {
            DirectoryInfo nextSubDir = target.CreateSubdirectory(subDir.Name);
            CopyDir(subDir, nextSubDir);
        }
    }

    public void OnClickRevert()
    {
        SceneManager.LoadSceneAsync(Scenes.Editor);
    }

    public void OnClickClear()
    {
        map.Clear();

        for (int i = 0; i < checkpointMarks.Count; i++)
        {
            VectorLine mark = checkpointMarks[i];
            VectorLine.Destroy(ref mark);
        }

        checkpoints.Clear();
        checkpointMarks.Clear();

        undoStack.Clear();
        redoStack.Clear();

        optionsPanel.SetActive(false);
        mainButtons.SetActive(true);

        PlayerPrefs.DeleteKey("EditCamX");
        PlayerPrefs.DeleteKey("EditCamY");
        PlayerPrefs.DeleteKey("EditCamZ");

        if (Directory.Exists(CurrentLevel))
            Directory.Delete(CurrentLevel, true);

        CurrentLevel = null;
        PlayerPrefs.DeleteKey("LastLevel");

        SceneManager.LoadScene(Scenes.Editor);
    }

    public void OnClickCloseOptions()
    {
        optionsPanel.SetActive(false);
        mainButtons.SetActive(true);
    }

    private void EnableAllIcons()
    {
        for (int i = 0; i < icons.Length; i++)
            icons[i].interactable = true;
    }

    public void SetNormalMode()
    {
        ClearSelections();
        EnableAllIcons();
        icons[0].interactable = false;
        mode = EditMode.Normal;
    }

    public void SetSelectionMode()
    {
        EnableAllIcons();
        icons[1].interactable = false;
        mode = EditMode.Selection;
    }

    public void SetCheckpointMode()
    {
        ClearSelections();
        EnableAllIcons();
        icons[2].interactable = false;
        mode = EditMode.Checkpoints;
    }

    public void SetFloodFillMode()
    {
        ClearSelections();
        EnableAllIcons();
        icons[3].interactable = false;
        mode = EditMode.FloodFill;
    }

    public void SetRectFillMode()
    {
        ClearSelections();
        EnableAllIcons();
        icons[4].interactable = false;
        mode = EditMode.RectFill;
    }

    private void Save(bool showMessage)
    {
        if (CurrentLevel == null)
            return;

        Directory.CreateDirectory(CurrentLevel);
        
        levelData.checkpoints = checkpoints;

        string json = JsonUtility.ToJson(levelData);
        byte[] data = Compress(json);

        File.WriteAllBytes(CurrentLevel + "/Data.txt", data);
        File.WriteAllBytes(CurrentLevel + "/Tiles.txt", map.Compress());

        if (showMessage) ShowMessage("Save successful");
    }

    private void GetLevels()
    {
        string[] dirs = Directory.GetDirectories(Application.persistentDataPath + "/World/");
        List<string> dirList = new List<string>(dirs);
        dirList.Sort((a, b) => Directory.GetCreationTime(a).CompareTo(Directory.GetLastWriteTime(b)));
        levelsInWorld = dirList.ToArray();
    }

    public void OnOpenLevels()
    {
        mainButtons.SetActive(false);
        levelsPanel.SetActive(true);

        GetLevels();
        FillLevelsPage();
    }

    public void OnClickCloseLevels()
    {
        levelsPanel.SetActive(false);
        mainButtons.SetActive(true);
    }

    private void FillLevelsPage()
    {
        Transform parent = levelsPanel.transform.Find("Levels");

        Button[] buttons = parent.GetComponentsInChildren<Button>();

        for (int i = 0; i < buttons.Length; i++)
        {
            GameObject obj = buttons[i].gameObject;

            if (!obj.CompareTag("NewLevel"))
                Destroy(buttons[i].gameObject);
        }

        int start = currentPage * LevelsPerPage;
        int end = Mathf.Min(start + LevelsPerPage, levelsInWorld.Length);

        for (int i = start; i < end; i++)
        {
            GameObject obj = Instantiate(levelButtonPrefab);
            obj.transform.SetParent(parent);

            string levelName = new DirectoryInfo(levelsInWorld[i]).Name;
            
            obj.transform.Find("Text").GetComponent<Text>().text = levelName;
            
            int buttonID = i;
            obj.GetComponent<Button>().onClick.AddListener(() => { SwitchLevel(buttonID); });
        }

        levelsPageUp.interactable = currentPage != 0;
        levelsPageDown.interactable = ((currentPage + 1) * LevelsPerPage) < levelsInWorld.Length;
    }

    public void NewLevel()
    {
        PlayerPrefs.SetString("ForceLevel", String.Empty);
        GameController.Instance.ChangeScene(Scenes.Editor);
    }

    public void SwitchLevel(int ID)
    {
        PlayerPrefs.SetString("ForceLevel", levelsInWorld[ID]);
        GameController.Instance.ChangeScene(Scenes.Editor);
    }

    public void LevelPageUp()
    {
        currentPage++;
        FillLevelsPage();
    }

    public void LevelPageDown()
    {
        currentPage--;
        FillLevelsPage();
    }

    private void ShowMessage(string text, float time = 2.0f)
    {
        StopAllCoroutines();
        StartCoroutine(ShowMessageRoutine(text, time));
    }

    private IEnumerator ShowMessageRoutine(string text, float time)
    {
        message.GetComponent<Text>().text = text;
        message.parent.gameObject.SetActive(true);
        yield return new WaitForSeconds(time);
        message.parent.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        this.MMEventStopListening<GameLoadedEvent>();
        this.MMEventStopListening<SceneChangeEvent>();
    }
}
