﻿// Adventure

public struct TileInstance
{
	public Tile tile;
	public Vector2i pos;

	public TileInstance(Tile tile, Vector2i pos)
	{
		this.tile = tile;
		this.pos = pos;
	}
}
