﻿// Adventure

using UnityEngine;
using System;

[Serializable]
public struct Vector2i : IEquatable<Vector2i>
{
	public int x, y;

	public static readonly Vector2i Up = new Vector2i(0, 1);
	public static readonly Vector2i Down = new Vector2i(0, -1);
	public static readonly Vector2i Left = new Vector2i(-1, 0);
	public static readonly Vector2i Right = new Vector2i(1, 0);

	public static readonly Vector2i[] Directions = { Vector2i.Up, Vector2i.Down, Vector2i.Left, Vector2i.Right };

	public Vector2i(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public Vector3 ToV3()
	{
		return new Vector3(x, y, 0.0f);
	}

	public override bool Equals(object other)
	{
		return Equals((Vector2i)other);
	}

	public bool Equals(Vector2i other)
	{
		return this.x == other.x && this.y == other.y;
	}

	public override int GetHashCode()
	{
		return x.GetHashCode() * 17 + y.GetHashCode();
	}

	public override string ToString()
	{
		return x + ", " + y;
	}

	public static Vector2i operator + (Vector2i a, Vector2i b) 
	{
		return new Vector2i(a.x + b.x, a.y + b.y);
	}

	public static Vector2i operator * (Vector2i a, int value) 
	{
		return new Vector2i(a.x * value, a.y * value);
	}

	public static bool operator == (Vector2i a, Vector2i b) 
	{
		return a.x == b.x && a.y == b.y;
	}

	public static bool operator != (Vector2i a, Vector2i b) 
	{
		return a.x != b.x || a.y != b.y;
	}
}
