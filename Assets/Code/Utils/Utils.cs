﻿// Adventure

using UnityEngine;
using System.IO;
using System.IO.Compression;

public static class Utils
{
	public static float Square(float value)
	{
		return value * value;
	}

	public static bool CtrlPressed()
	{
		return Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl);
	}
	
	public static byte[] Compress(string data)
    {
        using (MemoryStream output = new MemoryStream())
        {
            using (DeflateStream gzip = new DeflateStream(output, CompressionMode.Compress))
            {
                using (StreamWriter writer = new StreamWriter(gzip, System.Text.Encoding.UTF8))
                    writer.Write(data);
            }

            return output.ToArray();
        }
    }

	public static string Decompress(byte[] data)
	{
		using (MemoryStream inputStream = new MemoryStream(data))
		{
			using (DeflateStream gzip = new DeflateStream(inputStream, CompressionMode.Decompress))
			{
				using (StreamReader reader = new StreamReader(gzip, System.Text.Encoding.UTF8))
					return reader.ReadToEnd();
			}
		}
	}
}
