﻿// Adventure

using UnityEngine;
using MoreMountains.CorgiEngine;

public sealed class Player : Character 
{
	[SerializeField] private GameObject colliderPrefab;

	private Map map;
	private Level levelManager;

	private int colIndex = 0;
	private TileCollider[] colliders = new TileCollider[9];

	protected override void Awake()
	{
		base.Awake();

		map = Map.Instance;

		for (int i = 0; i < colliders.Length; i++)
			colliders[i] = GameObject.Instantiate(colliderPrefab).GetComponent<TileCollider>();
	}

	private void Start()
	{
		levelManager = Level.Instance;
	}

	public override void RespawnAt(Transform spawnPoint, FacingDirections facingDirection)
	{
       	Face(facingDirection);
		ConditionState.ChangeState(CharacterStates.CharacterConditions.Normal);
		GetComponent<Collider2D>().enabled = true;
		_controller.CollisionsOn();
		_health.ResetHealthToMaxHealth();
		_health.Revive();

        Vector3 pos = spawnPoint.position;
		float newX = Mathf.Floor(pos.x) + 0.5f;
        transform.position = new Vector3(newX, pos.y, pos.z);
	}

	public Vector3 Position
	{
		get { return transform.position; }
	}

	public void Kill()
	{
		levelManager.KillPlayer(this);
	}

	private void GenerateColliders()
	{
		Vector2i pos = map.WorldToTilePos(transform.position);

		for (int y = -1; y <= 1; y++)
		{
			for (int x = -1; x <= 1; x++)
			{
				int tX = pos.x + x, tY = pos.y + y;
				Tile tile = map.GetTileSafe(tX, tY);

				TileCollider col = colliders[colIndex];

				if (tile.HasCollider)
				{
					switch (tile.Collider)
					{
						case ColliderType.Box:
							if (x == 0 && y == 0)
								map.SetTile(tX, tY, TileType.Air);

							col.Move(tX + 0.5f, tY + 0.5f);
							col.EnableBox();
							break;

						case ColliderType.Slope:
							col.Move(tX, tY);
							col.EnableSlope();
							col.Rotate(tile.rotation);
							break;

						case ColliderType.Edge:
							col.Move(tX, tY);
							col.EnableEdge();
							break;

						case ColliderType.Trigger:
							col.Move(tX + 0.5F, tY + 0.5f);
							col.EnableTrigger();
							break;
					}
				}
				else col.Disable();

				colIndex = (colIndex + 1) % colliders.Length;
			}
		}
	}
		
	protected override void EveryFrame()
	{
		base.EveryFrame();

		GenerateColliders();

		if (ConditionState.CurrentState != CharacterStates.CharacterConditions.Dead && transform.position.y < -5.0f)
			Kill();
	}
}
