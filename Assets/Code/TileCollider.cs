﻿// Adventure

using UnityEngine;

public enum ColliderType { None = 0, Box = 1, Slope = 2, Edge = 3, Trigger = 4 }

public sealed class TileCollider : MonoBehaviour
{
	private Transform t;

	[SerializeField] private BoxCollider2D box;
	[SerializeField] private PolygonCollider2D slope;
	[SerializeField] private EdgeCollider2D edge;

	private int oneWay;
	private int standard;

	public ColliderType Type { get; private set; }

	private void Awake()
	{
		oneWay = LayerMask.NameToLayer("OneWayPlatform");
		standard = LayerMask.NameToLayer("Terrain");
		t = GetComponent<Transform>();
	}

	public void Move(float x, float y)
	{
		t.position = new Vector3(x, y);
	}

	public void Rotate(byte rot)
	{
		Vector3 p = t.position;

		switch (rot)
		{
			case 0:
				t.position = new Vector3(p.x, p.y, p.z);
				break;

			case 1:
				t.position = new Vector3(p.x + 1.0f, p.y, p.z);
				break;

			case 2:
				t.position = new Vector3(p.x, p.y, p.z);
				break;

			case 3:
				t.position = new Vector3(p.x, p.y, p.z);
				break;
		}

		t.rotation = Quaternion.Euler(0.0f, 0.0f, rot * 90.0f);
	}

	public void Adjust(float offsetX, float offsetY, float width, float height)
	{
		if (box.enabled)
		{
			t.localScale = new Vector3(width, height, 1.0f);
			t.position += new Vector3(offsetX, offsetY);
		}
		else if (edge.enabled)
			t.localScale = new Vector3(width, 1.0f, 1.0f);
		else t.localScale = Vector3.one;
	}

	public void EnableBox()
	{
		box.isTrigger = false;
		t.gameObject.layer = standard;
		slope.enabled = false;
		edge.enabled = false;
		box.enabled = true;
	}

	public void EnableSlope()
	{
		t.gameObject.layer = standard;
		box.enabled = false;
		edge.enabled = false;
		slope.enabled = true;
	}

	public void EnableEdge()
	{
		t.gameObject.layer = oneWay;
		box.enabled = false;
		slope.enabled = false;
		edge.enabled = true;
	}

	public void EnableTrigger()
	{
		EnableBox();
		box.isTrigger = true;
	}

	public void Disable()
	{
		box.enabled = false;
		slope.enabled = false;
		edge.enabled = false;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		Map.Instance.GetTile(Map.Instance.WorldToTilePos(t.position)).OnTrigger();
	}
}
