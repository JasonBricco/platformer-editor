﻿// Adventure

using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using System;

public delegate void ThreadWork(object data);

public sealed class ThreadManager : MonoBehaviour 
{
	private Queue<UnityAction> mainThreadWork = new Queue<UnityAction>();
	private WorkerThread[] threads;

	private int next = 0;

	public void Awake()
	{
		threads = new WorkerThread[Environment.ProcessorCount];

		for (int i = 0; i < threads.Length; i++)
			threads[i] = new WorkerThread();
	}

	private void Update()
	{
		if (mainThreadWork.Count > 0)
			mainThreadWork.Dequeue().Invoke();

		for (int i = 0; i < threads.Length; i++)
			threads[i].TrySetHandle();
	}

	public void QueueWork(ThreadWork task, object arg, bool isPriority)
	{
		next = (next + 1) % threads.Length;
		threads[next].QueueWork(task, arg, isPriority);
	}

	public void QueueForMainThread(UnityAction method)
	{
		mainThreadWork.Enqueue(method);
	}

	private void OnDestroy()
	{
		for (int i = 0; i < threads.Length; i++)
			threads[i].Stop();
	}
}
