﻿// Adventure

using UnityEngine;
using System.Collections.Generic;

public sealed class Chunk 
{
	public const int Size = 16, Shift = 4;

    private ChunkRenderer renderer;

	public Mesh mesh;
    public Vector2i chunkPos, worldPos;
	public bool loaded;
	public bool pendingUpdate;

	public Chunk(ChunkRenderer renderer, int x, int y)
	{
		this.renderer = renderer;
		chunkPos = new Vector2i(x, y);
        worldPos = new Vector2i(x * Size, y * Size);
	}

	public void Reset()
	{
		renderer.ReturnMesh(mesh);
		mesh = null;
		loaded = false;
		pendingUpdate = false;
	}
}
