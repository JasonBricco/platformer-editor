﻿// Adventure

using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System;
using System.Collections;

public sealed class Level : LevelManager, MMEventListener<GameLoadedEvent>
{
    public Player Player { get; private set; }

    public static new Level Instance { get; private set; }

    protected override void Awake()
    {
        base.Awake();
        Instance = this;
        this.MMEventStartListening();
    }

    public override void Start() {}

    public void OnMMEvent(GameLoadedEvent e)
    {
        LevelBounds.center = new Vector3(Map.SizeInTiles / 2, Map.SizeInTiles / 2);
		LevelBounds.extents = LevelBounds.center;

        if (e.data != null)
            Map.Instance.Load(e.data.levelPath);

        Initialization();
        LevelGUIStart();
        CheckpointAssignment();
        SpawnSingleCharacter();
        InstantiateNoGoingBack();

        Camera.main.GetComponent<GameCamera>().Init();
        MMEventManager.TriggerEvent(new CorgiEngineEvent(CorgiEngineEventTypes.LevelStart));

        Player = (Player)Players[0];
    }

    public override void KillPlayer(Character player)
    {
        player.GetComponent<Health>().Kill();
        MMEventManager.TriggerEvent(new CorgiEngineEvent(CorgiEngineEventTypes.PlayerDeath));
        StartCoroutine(RestartLevel());
    }

    public void KillPlayer()
    {
        KillPlayer(Player);
    }

    private IEnumerator RestartLevel()
    {
        LevelCameraController.FollowsPlayer = false;
        yield return new WaitForSeconds(RespawnDelay);

        if (CurrentCheckPoint != null)
            CurrentCheckPoint.SpawnPlayer(Players[0]);

        _started = DateTime.UtcNow;

        MMEventManager.TriggerEvent(new CorgiEnginePointsEvent(PointsMethods.Set, _savedPoints));
        MMEventManager.TriggerEvent(new CorgiEngineEvent(CorgiEngineEventTypes.Respawn));
    }

    private void OnDestroy()
    {
        this.MMEventStopListening();
    }
}
