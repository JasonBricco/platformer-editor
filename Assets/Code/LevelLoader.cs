﻿// Adventure

using UnityEngine;
using System;
using System.IO;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using static Utils;

public struct GameLoadedEvent
{
	public LevelData data;

	public GameLoadedEvent(LevelData data)
	{
		this.data = data;
	}
}

public sealed class LevelLoader : MonoBehaviour 
{
	private void Start()
	{	
		string worldPath = Application.persistentDataPath + "/World/";
		Directory.CreateDirectory(worldPath);
		
		string path = String.Empty;

        if (PlayerPrefs.HasKey("ForceLevel"))
        {
            path = PlayerPrefs.GetString("ForceLevel");
            PlayerPrefs.DeleteKey("ForceLevel");
        }
        else if (PlayerPrefs.HasKey("LastLevel"))
            path = PlayerPrefs.GetString("LastLevel");
        else
        {
            DateTime newestDate = new DateTime(1900, 1, 1);

            foreach (string dir in Directory.GetDirectories(worldPath))
            {
                DirectoryInfo info = new DirectoryInfo(dir);
                DateTime lastWrite = info.LastWriteTime;

                if (lastWrite > newestDate)
                {
                    path = dir;
                    newestDate = lastWrite;
                }
            }
        }

		LevelData data = null;

        if (!String.IsNullOrEmpty(path))
		{
			byte[] bytes = File.ReadAllBytes(path + "/Data.txt");
			string json = Decompress(bytes);
	
			data = JsonUtility.FromJson<LevelData>(json);
			data.levelPath = path + "/Tiles.txt";
			
			GameObject checkpoint = Resources.Load<GameObject>("Checkpoint");
				
			for (int i = 0; i < data.checkpoints.Count; i++)
			{
				Vector2i pos = data.checkpoints[i];
				Instantiate(checkpoint, new Vector3(pos.x + 0.25f, pos.y + 1.0f), Quaternion.identity);
			}
		}

		MMEventManager.TriggerEvent(new GameLoadedEvent(data));
	}
}
