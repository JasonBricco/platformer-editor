﻿// Adventure

using UnityEngine;
using System.Collections.Generic;

public sealed class ChunkRenderer : MonoBehaviour 
{
	[SerializeField] private Material[] materials;

	private Map map;
	private Queue<Mesh> meshPool = new Queue<Mesh>();

	List<Vector3> vertices = new List<Vector3>(4 * Map.TilesPerChunk);
	List<Vector3> uvs = new List<Vector3>(4 * Map.TilesPerChunk);
	List<int>[] triangles;

	private void Awake()
	{
		map = GameObject.FindWithTag("Map").GetComponent<Map>();

		triangles = new List<int>[materials.Length];

		for (int i = 0; i < triangles.Length; i++)
			triangles[i] = new List<int>(4 * Map.TilesPerChunk);
	}
	
	public Mesh GetMesh()
	{
		if (meshPool.Count > 0) return meshPool.Dequeue();
		else return new Mesh();
	}

	public void ReturnMesh(Mesh mesh)
	{
		meshPool.Enqueue(mesh);
	}

	public void BuildMesh(Chunk chunk)
	{
		if (chunk.mesh != null)
			ReturnMesh(chunk.mesh);
		
		chunk.mesh = GetMesh();

		vertices.Clear();
		uvs.Clear();
		
		for (int i = 0; i < triangles.Length; i++)
			triangles[i].Clear();

        Vector2i wPos = chunk.worldPos;

		for (int y = 0; y < Chunk.Size; y++)
		{
			for (int x = 0; x < Chunk.Size; x++)
			{
				Tile tile = map.GetTile(wPos.x + x, wPos.y + y);

				if (tile.Visible)
				{
					int material = tile.Material;
					int offset = vertices.Count;

					triangles[material].Add(offset + 2);
					triangles[material].Add(offset + 1);
					triangles[material].Add(offset + 0);

					triangles[material].Add(offset + 3);
					triangles[material].Add(offset + 2);
					triangles[material].Add(offset + 0);

					vertices.Add(new Vector3(x + 1.0f, y));
					vertices.Add(new Vector3(x + 1.0f, y + 1.0f));
					vertices.Add(new Vector3(x, y + 1.0f));
					vertices.Add(new Vector3(x, y));

					if (tile.rotation != 0)
					{
						Quaternion rot = Quaternion.Euler(0.0f, 0.0f, tile.rotation * 90);
						Vector3 center = new Vector3(x + 0.5f, y + 0.5f);

						for (int i = vertices.Count - 4; i < vertices.Count; i++)
							vertices[i] = rot * (vertices[i] - center) + center;
					}
					
					float tex = tile.GetTexture();

					uvs.Add(new Vector3(1.0f, 0.0f, tex));
					uvs.Add(new Vector3(1.0f, 1.0f, tex));
					uvs.Add(new Vector3(0.0f, 1.0f, tex));
					uvs.Add(new Vector3(0.0f, 0.0f, tex));
				}
			}
		}

		chunk.mesh.Clear(false);

		chunk.mesh.SetVertices(vertices);
		chunk.mesh.subMeshCount = materials.Length;

		for (int i = 0; i < triangles.Length; i++)
			chunk.mesh.SetTriangles(triangles[i], i);

		chunk.mesh.SetUVs(0, uvs);
	}

	public void Render(List<Chunk> chunksToRender)
	{
		for (int i = 0; i < chunksToRender.Count; i++)
		{
            Chunk chunk = chunksToRender[i];

			for (int j = 0; j < chunk.mesh.subMeshCount; j++)
				Graphics.DrawMesh(chunk.mesh, chunk.worldPos.ToV3(), Quaternion.identity, materials[j], 0, null, j, null, false, false);
		}
	}
}
